from flask_restful import Api, Resource , reqparse
from flask import Flask 
from langdetect import detect, detect_langs ,DetectorFactory

DetectorFactory.seed =0

app=Flask(__name__)
api = Api(app)

data_put_args=reqparse.RequestParser()
data_put_args.add_argument("text",type=str ,help = "string to detect his language", required=True)


class Detect (Resource):
	def put(self):
		args=data_put_args.parse_args()
		langs=detect_langs(args['text'])
		data={
				"lang":{ 
						"highestScore":str(langs[0]).split(":")[0] , 
						"scores" :{str(langs[i]).split(":")[0] : str(langs[i]).split(":")[1] for i in range(len(langs)) }
						}
			}
		return data


api.add_resource(Detect,"/language_detect")
if __name__=="__main__" :
	app.run(debug=True,host='0.0.0.0', port=5000)
		
