FROM python:3
ADD detect_lang.py /
RUN pip install flask_restful
RUN pip install flask
RUN pip install langdetect
CMD ["python","./detect_lang.py"]
